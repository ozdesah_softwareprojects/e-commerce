﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ETic.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 800px;
            height: 100px;
        }
        
        .auto-style3 {
            border-color: #FF9900;
            background-color: #FFFF99;
        }
        .auto-style5 {
            width: 140px;
        }

        .visibility{
            display: block;
            visibility:hidden;
            background-color:sandybrown;
        }
        
        
        
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table align="center">
            <tr>
                <td>
                    <img alt="" class="auto-style1" src="Başlıksız.png" /></td>
            </tr>
        </table>

    </div>
        <table class ="auto-style3" align="center" >
            <tr>
                <td class="auto-style5">Kategoriye göre ara<asp:DropDownList ID="DropDownList1" runat="server" Width="125px" AutoPostBack="True" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
                    
                    <asp:ListItem>Hepsi</asp:ListItem>
                    <asp:ListItem>Ates</asp:ListItem>
                    <asp:ListItem>Hava</asp:ListItem>
                    <asp:ListItem>Su</asp:ListItem>
                    <asp:ListItem>Toprak</asp:ListItem>
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:Library %>" SelectCommand="SELECT * FROM [UrunTable]"></asp:SqlDataSource>
                </td>
                <td align="center">ÜRÜNLER</td>
            </tr>
            <tr>
                <td align="center"><a href="Default.aspx">Ana Sayfa</a><br />
                    <br />
                    <a href="Sepet.aspx" aria-orientation="vertical">Sepetim</a></td>

                <td>
            
                    
            
                    
            
                    
            
            <asp:DataList ID="DataList1" runat="server" RepeatColumns="5" RepeatDirection="Horizontal">        
            <ItemTemplate>
                
                   <td>
                   <div style="margin-left: auto; margin-right: auto; text-align: center;">
                   <asp:Label ID="Label1" runat="server" BorderColor="#660033" BackColor="White" Height="23px" Width="180px" BorderStyle="Solid">
                    
                   <%#Eval("Ad") %>
                        <asp:HiddenField ID="HiddenFieldAd" runat="server" Value='<%#Eval("Ad")%>' /> <asp:LinkButton ID="LinkButtonDetay" runat="server" OnClick="LinkButtonDetay_Click"> [Detay]</asp:LinkButton>
                        
                    </asp:Label>
                    </div>
                 
                    
                     <asp:Label ID="LabelDetay" runat="server" class="visibility" BorderColor="#660033" Height="150px" Width="180px" BorderStyle="Solid">
                        <div style="text-align: center;">
                            <b>Fiyat:</b> <%#Eval("Fiyat") %> TL <hr />
                            <asp:HiddenField ID="HiddenFieldFiyat" runat="server" Value='<%#Eval("Fiyat")%>' />
                        </div>
                            <%#Eval("Aciklama") %> <br /> <br />
                            <b>Kategori: </b><%#Eval("Kategori") %><br /> 
                            <asp:Label ID="LabelMiktar" runat="server" Text=""></asp:Label>
                    </asp:Label>
                 
                       
                    <div style="margin-left: auto; margin-right: auto; text-align: center;">
                    <asp:TextBox ID="MiktarTextBox" runat="server">1</asp:TextBox> <br />   
                    <asp:LinkButton ID="LinkButtonSepeteEkle" runat="server" OnClick="LinkButtonSepeteEkle_Click">Sepete Ekle</asp:LinkButton>
                       
                       
                        <br />
                       
                </div>
                 <br />
                   </td>
                  
                              
            </ItemTemplate>
                
                 </asp:DataList> 
             </td>
            </tr>
        </table>
    </form>
</body>
</html>
